"""
Code that goes along with the Airflow located at:
http://airflow.readthedocs.org/en/latest/tutorial.html
"""
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
from airflow.contrib.hooks import SSHHook
from airflow.contrib.operators.ssh_operator import SSHOperator

default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": datetime(2015, 6, 1),
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 1,
    "retry_delay": timedelta(minutes=1),
}


#sshHook = SSHHook(ssh_conn_id='store_tech_dev')


dag = DAG(
    dag_id = "Remote_Command_Demo", 
    description = "Demoing running a command on the store tech development box",
    default_args = default_args, 
    schedule_interval = timedelta(minutes=20)
)


t1_bash = "bash /home/rcomm/call_from_airflow.sh"


connect_ssh = SSHOperator(
    task_id = "task1",
    ssh_conn_id = "store_tech_dev",
    command = t1_bash,
    dag = dag
)
