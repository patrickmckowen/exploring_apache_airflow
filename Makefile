

@PHONY: up_interactive
up_celery_interactive:
	docker-compose -f docker-compose-LocalExecutor.yml up

@PHONY: up_celery
up_celery:
	docker-compose -d -f docker-compose-LocalExecutor.yml up
